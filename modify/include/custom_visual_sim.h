/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *    visual_sim.h
 *
 * Project:
 * --------
 *   Maui_Software
 *
 * Description:
 * ------------
 *   This file is used for including files for eSIM
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#define SIM_DUMMY_API_L1		1

/* not define SIM_DUMMY_API_L1    MTK Code
          define  SIM_DUMMY_API_L1   eSIM Code
*/

/* define GSM_SIM data Structure */
typedef struct
{
	kal_bool			bCHV1;
	kal_bool			bCHV2;
	kal_bool			bUnblock_CHV1;
	kal_bool			bUnblock_CHV2;

	kal_uint8			byCurrent_Select_Index;
	kal_uint8			byCurrent_Select_File_Type;
	kal_uint8			byCurrent_File_Structure;
	kal_uint8			byFile_Record_Length;
	kal_uint8			byFile_Record_Operate_Index;
	kal_uint16      		unFile_Size;
	kal_uint16             unEF_DIR_ID;
	const kal_uint8      *pFile_EF_DIR_Base;
	const kal_uint8      *pFile_Base;
}GSM_SIM_Run_Status_Struct;

#define Visual_SIM_Nor_Flash_Data_Size		0x10000

#define Sector_First_Size			0x0200
#define Sector_FAT_Size				0x0D00
#define Sector_Data_Size			0x7100
#define Sector_Backup_Size			0x8000

#define Visual_SIM_Nor_Flash_Backup_Size	MTK2503_Nor_Flash_Block_Size

typedef struct
{
	kal_uint8			bySector_First[Sector_First_Size];
	kal_uint8			bySector_FAT[Sector_FAT_Size];
	kal_uint8			bySector_Data[Sector_Data_Size];
  	kal_uint8			bySector_Backup[Sector_Backup_Size];
}Nor_Flash_Struct;


typedef struct
{
	kal_uint8			byVisual_Sim_Ready_Flag[0x10];
}Sector_First_Struct;

extern GSM_SIM_Run_Status_Struct strGSM_SIM_Run_Status;

#define Push_strGSM_SIM_Run_Status      	GSM_SIM_Run_Status_Struct strTemp_GSM_SIM_Run_Status;		kal_mem_cpy(&strTemp_GSM_SIM_Run_Status, &strGSM_SIM_Run_Status, sizeof(GSM_SIM_Run_Status_Struct))
#define Pop_strGSM_SIM_Run_Status		kal_mem_cpy(&strGSM_SIM_Run_Status, &strTemp_GSM_SIM_Run_Status, sizeof(GSM_SIM_Run_Status_Struct))


extern kal_bool sim_dummyAPI_Visual_Return(void);
extern sim_status sim_dummyAPI_L1sim_Cmd_All(kal_uint8  *txData,kal_uint32  *txSize,kal_uint8  *rxData, kal_uint32  *rxSize, SIM_ICC_APPLICATION application);
extern usim_status_enum sim_dummyAPI_L1sim_Reset_All(sim_power_enum ExpectVolt, sim_power_enum *ResultVolt, kal_bool warm, SIM_ICC_APPLICATION application);
extern usim_status_enum sim_dummyAPI_L1sim_Reset_All_HW_verification(sim_power_enum ExpectVolt, sim_power_enum *ResultVolt, kal_bool warm, kal_uint32 simInterface);
extern void sim_dummyAPI_L1sim_PowerOff_All(SIM_ICC_APPLICATION application);
extern void sim_dummyAPI_L1sim_Get_Card_Info_All(sim_info_struct *info, SIM_ICC_APPLICATION application);
extern void sim_dummyAPI_L1sim_Enable_Enhanced_Speed_All(kal_bool enable, SIM_ICC_APPLICATION application);
extern void sim_dummyAPI_L1sim_Select_Prefer_PhyLayer_All(sim_protocol_phy_enum T, SIM_ICC_APPLICATION application);
extern kal_bool sim_dummyAPI_L1sim_Set_ClockStopMode_All(sim_clock_stop_enum mode, SIM_ICC_APPLICATION application);
extern sim_card_speed_type sim_dummyAPI_L1sim_Get_CardSpeedType(SIM_ICC_APPLICATION application);

extern void GSM_SIM_Run_Status_Init(void);

